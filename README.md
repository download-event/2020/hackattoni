# Download Hackathon 2020 team Hackattoni
Our project consists in determining the mood of group chats through a [Telegram Bot](https://t.me/HackattoniBot).
The idea behind the project is that we already have hundreds of social network and instant messaging apps, and thus chat groups are now part of our everyday life. While we spend more and more time chatting with strangers, we slowly forget to be in a social contest. 
Such a situation relies on social feedback, which is not so apparent on a digital platform. 
Our model, based on deep learning, has been trained to determine the current mood of the group chat (based on the messages sent in the last minutes). We think that such a technology could open the way to interesting developments to make group chats less digital and more real.


### How do I use it?
You just have to add the `@HackattoniBot` to your Telegram group chat!
But remember: the neural network is trained only in English.

We are currently hosting the telegram bot on our server for you to try it.
So, if you want to run the script locally, remember to add a new token ;)

You can train, generate and save your own instance of the neural network by running the sentiment-analysis notebook. It can later be tested with the analizer notebook.
If you don't have time to train it, you can just download our copy here:
https://drive.google.com/file/d/1wmXM5Y0E4EdopxXyvuXWcl34BTKbeJM6/view?usp=sharing
(Due to its complexity, it was too heavy to upload it here...)

### How will I know if the chat mood is positive or negative?
You will have to send the command "/report" in the group chat and the Bot will answer with a straightforward visual indicator!