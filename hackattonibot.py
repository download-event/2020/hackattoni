import sys
import time
import telepot
from telepot.loop import MessageLoop
from tensorflow.keras.models import load_model
import joblib
from datetime import datetime

def report(chat_id, last_ten):

  yellow_dot = '\U0001F7E1'
  green_dot = '\U0001F7E2'
  red_dot = '\U0001F534'

  current_mood = sum([x[0] for x in last_ten])/len(last_ten)

  if current_mood > 0.7:
    signal = green_dot
  elif current_mood > 0.4:
    signal = yellow_dot
  else:
    signal = red_dot

  bot.sendMessage(chat_id, signal)

def analyze(s, last_ten):
  mood = (model.predict(vectorizer.transform([s]).todense()))[0][0]
  last_ten.append([mood, datetime.now()])
  current_time = datetime.now()
  last_ten = [x for x in last_ten if ((current_time-x[1]).total_seconds()/60)<10]


def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(content_type, chat_type, chat_id)

    if (chat_type != 'group'):
      bot.sendMessage(chat_id, 'Use the bot in a group chat!')
      print("Messaggio ignorato")
      return

    if content_type == 'text':
      message = msg['text']
      if message == '/start':
        bot.sendMessage(chat_id, 'Welcome!')
      if message == '/report':
        report(chat_id, last_ten)
      else:
        analyze(message, last_ten)


TOKEN = '1150544523:AAFWba9Yu7CQZ3Ucuaoh0eGY7Pq3w3wbLFQ'

vectorizer = joblib.load('vectorizer.sav')
model = load_model('sent_anal.h5', compile = False)
last_ten = []
last_ten.append([0, datetime.now()])
bot = telepot.Bot(TOKEN)
MessageLoop(bot, handle).run_as_thread()
print ('In ascolto...')

while 1:
    time.sleep(10)
